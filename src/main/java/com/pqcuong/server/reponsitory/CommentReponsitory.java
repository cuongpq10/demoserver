package com.pqcuong.server.reponsitory;

import com.pqcuong.server.model.database.Comment;
import com.pqcuong.server.model.response.CommentResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentReponsitory extends JpaRepository<Comment,Integer> {
    @Query(nativeQuery = true,value = "SELECT comment.*, user_profile.falname, user_profile.avatar FROM comment " +
            "JOIN user_profile ON comment.id_user=user_profile.id_user WHERE id_news=:id_news LIMIT 1")
    Comment getComment(@Param("id_news") int id_news);

    @Query(nativeQuery = true,value = "SELECT comment.*, user_profile.falname, user_profile.avatar FROM comment " +
            "JOIN user_profile ON comment.id_user=user_profile.id_user WHERE id_comment=:id_comment LIMIT 1")
    Comment findOneById(@Param(value = "id_comment")int id_comment);
}
