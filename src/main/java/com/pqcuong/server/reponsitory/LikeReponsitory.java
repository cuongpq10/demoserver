package com.pqcuong.server.reponsitory;

import com.pqcuong.server.model.database.NewLike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LikeReponsitory extends JpaRepository<NewLike,Integer> {
    @Query(nativeQuery = true,value = "Select new_like.* from new_like where new_like.id_new=:id_new")
    List<NewLike> findAllLikeByIdNew(@Param(value = "id_new")int id_new);

}
