package com.pqcuong.server.reponsitory;

import com.pqcuong.server.model.database.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationReponsitory extends JpaRepository<Notification, Integer> {
    @Query(nativeQuery=true,value = "SELECT * FROM notification WHERE id_user=:id_user")
    List<Notification> findAllNotifi(@Param(value = "id_user")int id_user);
}
