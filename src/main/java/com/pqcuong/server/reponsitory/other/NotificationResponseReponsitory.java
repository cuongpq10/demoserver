package com.pqcuong.server.reponsitory.other;

import com.pqcuong.server.model.response.NotificationResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationResponseReponsitory extends JpaRepository<NotificationResponse,Integer> {
    @Query(nativeQuery = true,value = "select notification.*, user_profile.falname, user_profile.avatar " +
            "from notification join user_profile on notification.id_friend=user_profile.id_user " +
            "where notification.id_user=:id_user order by id_notifi")
    List<NotificationResponse> findAllNotifiByIdUser(@Param(value = "id_user")int id_user);
}
