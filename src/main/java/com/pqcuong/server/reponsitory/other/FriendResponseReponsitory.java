package com.pqcuong.server.reponsitory.other;

import com.pqcuong.server.model.response.FriendResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendResponseReponsitory extends JpaRepository<FriendResponse,Integer> {
    @Query(nativeQuery = true,value = "SELECT friend.*, user_profile.falname,user_profile.avatar,user_profile.phone, " +
            "user_profile.country FROM friend JOIN user_profile " +
            "ON friend.id_ufriend=user_profile.id_user WHERE friend.id_user=:id_user and id_ufriend!=:id_user and friend.check_request=:check_request")
    List<FriendResponse> findAllMyFriend(@Param(value = "id_user")int id_user, @Param(value = "check_request")int check_request);

    @Query(nativeQuery = true,value = "SELECT friend.*, user_profile.falname,user_profile.avatar,user_profile.phone, " +
            "user_profile.country FROM friend JOIN user_profile " +
            "ON friend.id_user=user_profile.id_user WHERE friend.id_user!=:id_ufriend and id_ufriend=:id_ufriend and friend.check_request=:check_request")
    List<FriendResponse> findAllMyRequest(@Param(value = "id_ufriend")int id_ufriend, @Param(value = "check_request")int check_request);
}
