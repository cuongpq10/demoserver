package com.pqcuong.server.reponsitory.other;

import com.pqcuong.server.model.database.NewLike;
import com.pqcuong.server.model.response.LikeResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LikeResponseReponsitory extends JpaRepository<LikeResponse,Integer> {
    @Query(nativeQuery = true,value = "Select new_like.*, user_profile.falname, user_profile.avatar from new_like " +
            "join user_profile on new_like.id_user = user_profile.id_user " +
            "where new_like.id_new=:id_new")
    List<LikeResponse> findAllLikeById(@Param(value = "id_new")int id_new);

    @Query(nativeQuery = true,value = "Select new_like.*, user_profile.falname, user_profile.avatar from new_like " +
            "join user_profile on new_like.id_user = user_profile.id_user ")
    List<LikeResponse> findAllLike();
}
