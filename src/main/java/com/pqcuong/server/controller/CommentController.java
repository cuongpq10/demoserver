package com.pqcuong.server.controller;

import com.pqcuong.server.model.database.Comment;
import com.pqcuong.server.model.database.NewsStatus;
import com.pqcuong.server.model.request.AddCommentRequest;
import com.pqcuong.server.model.request.CommentRequest;
import com.pqcuong.server.model.response.BaseResponse;
import com.pqcuong.server.model.response.CommentResponse;
import com.pqcuong.server.reponsitory.CommentReponsitory;
import com.pqcuong.server.reponsitory.NewStatusReponsitory;
import com.pqcuong.server.reponsitory.other.CommentResponseReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
public class CommentController {

    @Autowired
    private CommentResponseReponsitory commentResponseReponsitory;
    @Autowired
    private CommentReponsitory commentReponsitory;
    @Autowired
    private NewStatusReponsitory newStatusReponsitory;
    @PostMapping("/api/getAllCommentByNewId")
    public Object getAllCommentByUserId(@RequestBody CommentRequest request){
        List<CommentResponse> commentResponse = commentResponseReponsitory.findAllByUserId(request.getId_news());
        if (commentResponse==null||commentResponse.size()==0){
            return new BaseResponse(false,"This new doesn't have comment");
        }
        return new BaseResponse(commentResponse);
    }
    @PostMapping("/api/addNewsComment")
    public Object addNewsComment(@RequestBody CommentRequest request){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.now();
        NewsStatus newsStatus = newStatusReponsitory.findOneByIdNew(request.getId_news());
        int numberComment = newsStatus.getComment_status();
        numberComment++;
        newsStatus.setComment_status(numberComment);
        newStatusReponsitory.save(newsStatus);
        Comment comment = new Comment();
        comment.setId_news(request.getId_news());
        comment.setId_user(request.getId_user());
        comment.setContent(request.getContent());
        comment.setNumberlike(request.getNumberlike());
        comment.setTimecm(localDateTime.toString());
        commentReponsitory.save(comment);
        return new BaseResponse(comment);
    }
    @PostMapping("/api/deleteCommentById")
    public Object deleteNewComment(@RequestBody CommentRequest request){
        NewsStatus newsStatus = newStatusReponsitory.findOneByIdNew(request.getId_news());
        if (request.getId_comment()==0&&request.getId_news()==0){
            return new BaseResponse(false,"ID_COMMENT is not found");
        }
        Comment comment = commentReponsitory.findOneById(request.getId_comment());
        if (comment==null){
            return new BaseResponse(false,"Delete fail");
        }
        int numberComment = newsStatus.getComment_status();
        numberComment--;
        newsStatus.setComment_status(numberComment);
        newStatusReponsitory.save(newsStatus);
        commentReponsitory.deleteById(comment.getId_comment());
        return new BaseResponse(comment);
    }
}
