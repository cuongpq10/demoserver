package com.pqcuong.server.controller;

import com.pqcuong.server.model.database.Friend;
import com.pqcuong.server.model.database.NewsStatus;
import com.pqcuong.server.model.database.Notification;
import com.pqcuong.server.model.request.GetOneNewRequest;

import com.pqcuong.server.model.request.NewStatusRequest;
import com.pqcuong.server.model.response.BaseResponse;
import com.pqcuong.server.model.response.NewDetailResponse;
import com.pqcuong.server.model.response.NewStatusResponse;
import com.pqcuong.server.reponsitory.FriendReponsitory;
import com.pqcuong.server.reponsitory.NewStatusReponsitory;
import com.pqcuong.server.reponsitory.NotificationReponsitory;
import com.pqcuong.server.reponsitory.other.CommentResponseReponsitory;
import com.pqcuong.server.reponsitory.other.NewDetailResponseReponsitory;
import com.pqcuong.server.reponsitory.other.NewStatusResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
public class NewController {
    @Autowired
    private NewStatusResponseRepository newStatusResponseRepository;
    @Autowired
    private NewStatusReponsitory newStatusReponsitory;
    @Autowired
    private NewDetailResponseReponsitory newDetailResponseReponsitory;
    @Autowired
    private CommentResponseReponsitory commentResponseReponsitory;
    @Autowired
    private FriendReponsitory friendReponsitory;
    @Autowired
    private NotificationReponsitory notificationReponsitory;
    @PostMapping("/api/getOneNewById")

    public Object getOneNewById(@RequestBody GetOneNewRequest request) {
        NewDetailResponse newsStatus  = newDetailResponseReponsitory.findOneByIdNew(request.getId_new());
        if (newsStatus == null) {
            return new BaseResponse(false, "doesn't have any your news!");
        }
        return new BaseResponse(newsStatus);
    }

    @PostMapping("/api/getAllNewByIdUser")
    public Object getAllNewByIdUser(@RequestBody NewStatusRequest request) {
        List<NewStatusResponse> newsStatus = newStatusResponseRepository.findAllStatusNews(request.getId_user());

        if (newsStatus.size() == 0 || newsStatus == null) {
            return new BaseResponse(false, "doesn't have any your news!");
        }
        return new BaseResponse(newsStatus);
    }

    @PostMapping("/api/insertNewById")
    public Object insertNewById(@RequestParam("file") MultipartFile file, @RequestParam("id_user") int id_user,@RequestParam("content")String content) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        NewsStatus newsStatus = new NewsStatus();

        LocalDateTime localDateTime = LocalDateTime.now();
        newsStatus.setTimenew(localDateTime.toString());
        String currentTime = localDateTime.toString().replace("/", "");
        currentTime = currentTime.replace(" ", "");
        currentTime = currentTime.replace(".", "");
        currentTime = currentTime.replace("-", "");
        currentTime = currentTime.replace(":", "");
        String fileName = currentTime+file.getOriginalFilename();
        long fileSize = file.getSize()/1024;
        File convertFile = new File("C:\\xampp\\htdocs\\android\\upload\\news\\"+fileName);
        try {
            convertFile.createNewFile();
            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(file.getBytes());
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        newsStatus.setId_user(id_user);
        newsStatus.setContent(content);
        newsStatus.setLink_image("http://192.168.167.100:81/android/upload/news/"+fileName);
        newsStatus.setLike_status(0);
        newsStatus.setComment_status(0);
        newStatusReponsitory.save(newsStatus);

        //add notification
        NewStatusRequest newsPostRequest = new NewStatusRequest();
        newsPostRequest.setId_user(id_user);
        NewDetailResponse newsPost  = newDetailResponseReponsitory.findOneNewByIdUser(newsPostRequest.getId_user());
        List<Friend> friend = friendReponsitory.findAllByIdUser(newsPostRequest.getId_user());
        if (friend.size()==0){
            return new BaseResponse(false,"add Notification fail!");
        }
        for (Friend friend1 : friend) {
            if (friend1.getId_user()!=friend1.getId_ufriend()){
                Notification notification = new Notification();
                notification.setId_new(newsPost.getId_new());
                notification.setId_friend(id_user);
                notification.setId_user(friend1.getId_ufriend());
                notification.setTime_notifi(localDateTime.toString());
                notification.setCheck_notifi(1);
                notificationReponsitory.save(notification);
            }
        }
        return new BaseResponse(true, "Post Status is Success");
    }
}
