package com.pqcuong.server.model.request;

public class GetOneNewRequest {
    private int id_new;

    public int getId_new() {
        return id_new;
    }

    public void setId_new(int id_new) {
        this.id_new = id_new;
    }
}
