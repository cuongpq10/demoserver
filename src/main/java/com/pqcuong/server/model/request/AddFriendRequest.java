package com.pqcuong.server.model.request;

public class AddFriendRequest {
    private int id_user;
    private int id_ufriend;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_ufriend() {
        return id_ufriend;
    }

    public void setId_ufriend(int id_ufriend) {
        this.id_ufriend = id_ufriend;
    }
}
