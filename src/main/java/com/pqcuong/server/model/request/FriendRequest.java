package com.pqcuong.server.model.request;

public class FriendRequest {
    private int id_user;
    private int id_ufriend;
    private int check_request;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getCheck_request() {
        return check_request;
    }

    public void setCheck_request(int check_request) {
        this.check_request = check_request;
    }

    public int getId_ufriend() {
        return id_ufriend;
    }

    public void setId_ufriend(int id_ufriend) {
        this.id_ufriend = id_ufriend;
    }
}
