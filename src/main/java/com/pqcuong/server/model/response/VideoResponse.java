package com.pqcuong.server.model.response;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class VideoResponse {
    @Id
    private int id_video;
    private String content;
    private String link_video;
    private String like_status;
    private String comment_status;
    private String time_upload;
    private String falname;
    private String avatar;
    private int id_user;
    private int id_ufriend;

    public int getId_video() {
        return id_video;
    }

    public void setId_video(int id_video) {
        this.id_video = id_video;
    }

    public String getContent() {
        return content;
    }

    public String getTime_upload() {
        return time_upload;
    }

    public void setTime_upload(String time_upload) {
        this.time_upload = time_upload;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLink_video() {
        return link_video;
    }

    public void setLink_video(String link_video) {
        this.link_video = link_video;
    }

    public String getLike_status() {
        return like_status;
    }

    public void setLike_status(String like_status) {
        this.like_status = like_status;
    }

    public String getComment_status() {
        return comment_status;
    }

    public void setComment_status(String comment_status) {
        this.comment_status = comment_status;
    }



    public String getFalname() {
        return falname;
    }

    public void setFalname(String falname) {
        this.falname = falname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_ufriend() {
        return id_ufriend;
    }

    public void setId_ufriend(int id_ufriend) {
        this.id_ufriend = id_ufriend;
    }
}
