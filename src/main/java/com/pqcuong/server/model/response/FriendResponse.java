package com.pqcuong.server.model.response;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class FriendResponse {
    @Id
    private int id_friend;
    private int id_user;
    private int id_ufriend;
    private String timeadd;
    private String falname;
    private String avatar;
    private String phone;
    private String country;
    private int check_request;


    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_friend() {
        return id_friend;
    }

    public void setId_friend(int id_friend) {
        this.id_friend = id_friend;
    }

    public int getId_ufriend() {
        return id_ufriend;
    }

    public void setId_ufriend(int id_ufriend) {
        this.id_ufriend = id_ufriend;
    }

    public String getTimeadd() {
        return timeadd;
    }

    public void setTimeadd(String timeadd) {
        this.timeadd = timeadd;
    }

    public String getFalname() {
        return falname;
    }

    public void setFalname(String falname) {
        this.falname = falname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getCheck_request() {
        return check_request;
    }

    public void setCheck_request(int check_request) {
        this.check_request = check_request;
    }
}
