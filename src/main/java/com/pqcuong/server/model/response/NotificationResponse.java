package com.pqcuong.server.model.response;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class NotificationResponse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_notifi;
    private int id_user;
    private int id_friend;
    private int id_new;
    private int id_comment;
    private int id_add_friend;
    private int check_notifi;
    private String time_notifi;
    private String falname;
    private String avatar;

    public int getId_notifi() {
        return id_notifi;
    }

    public void setId_notifi(int id_notifi) {
        this.id_notifi = id_notifi;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_friend() {
        return id_friend;
    }

    public void setId_friend(int id_friend) {
        this.id_friend = id_friend;
    }

    public int getId_new() {
        return id_new;
    }

    public void setId_new(int id_new) {
        this.id_new = id_new;
    }

    public int getId_comment() {
        return id_comment;
    }

    public void setId_comment(int id_comment) {
        this.id_comment = id_comment;
    }

    public int getId_add_friend() {
        return id_add_friend;
    }

    public void setId_add_friend(int id_add_friend) {
        this.id_add_friend = id_add_friend;
    }

    public int getCheck_notifi() {
        return check_notifi;
    }

    public void setCheck_notifi(int check_notifi) {
        this.check_notifi = check_notifi;
    }

    public String getTime_notifi() {
        return time_notifi;
    }

    public void setTime_notifi(String time_notifi) {
        this.time_notifi = time_notifi;
    }

    public String getFalname() {
        return falname;
    }

    public void setFalname(String falname) {
        this.falname = falname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
