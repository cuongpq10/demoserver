package com.pqcuong.server.model.response;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class NewDetailResponse {
    @Id
    private int id_new;
    private String content;
    private String link_image;
    private int like_status;
    private int comment_status;
    private String timenew;
    private String falname;
    private String avatar;

    public int getId_new() {
        return id_new;
    }

    public void setId_new(int id_new) {
        this.id_new = id_new;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLink_image() {
        return link_image;
    }

    public void setLink_image(String link_image) {
        this.link_image = link_image;
    }

    public int getLike_status() {
        return like_status;
    }

    public void setLike_status(int like_status) {
        this.like_status = like_status;
    }

    public int getComment_status() {
        return comment_status;
    }

    public void setComment_status(int comment_status) {
        this.comment_status = comment_status;
    }

    public String getTimenew() {
        return timenew;
    }

    public void setTimenew(String timenew) {
        this.timenew = timenew;
    }

    public String getFalname() {
        return falname;
    }

    public void setFalname(String falname) {
        this.falname = falname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
