package com.pqcuong.server.model.database;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "friend")
public class Friend {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_friend;
    private int id_user;
    private int id_ufriend;
    private String timeadd;
    private int check_request;

    public int getId_ufriend() {
        return id_ufriend;
    }

    public void setId_ufriend(int id_ufriend) {
        this.id_ufriend = id_ufriend;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_friend() {
        return id_friend;
    }

    public void setId_friend(int id_friend) {
        this.id_friend = id_friend;
    }

    public String getTimeadd() {
        return timeadd;
    }

    public void setTimeadd(String timeadd) {
        this.timeadd = timeadd;
    }

    public int getCheck_request() {
        return check_request;
    }

    public void setCheck_request(int check_request) {
        this.check_request = check_request;
    }
}
