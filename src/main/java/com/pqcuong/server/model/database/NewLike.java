package com.pqcuong.server.model.database;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class NewLike {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_like;
    private int id_new;
    private int id_user;
    private String time_like;

    public int getId_like() {
        return id_like;
    }

    public void setId_like(int id_like) {
        this.id_like = id_like;
    }

    public int getId_new() {
        return id_new;
    }

    public void setId_new(int id_new) {
        this.id_new = id_new;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getTime_like() {
        return time_like;
    }

    public void setTime_like(String time_like) {
        this.time_like = time_like;
    }
}
